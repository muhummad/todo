<!DOCTYPE html>

<html>
  <head>
    <title>Login</title>
    <style type="text/css">
      .label {text-align: right}
      .error {color: red}
    </style>

  </head>

  <body>
    <h2>Add a new item:</h2>

    <form method="post">
      <table>

        <tr>
          <td class="label">Description</td>
          <td>
            <input type="text" name="description" placeholder="What do you need to do?" value="">
          </td>
        </tr>

      </table>

      <input type="submit">
    </form>

  </body>
</html>