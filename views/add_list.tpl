<!DOCTYPE html>

<html>
  <head>
    <title>Login</title>
    <style type="text/css">
      .label {text-align: right}
      .error {color: red}
    </style>

  </head>

  <body>
    <h2>Add a new list:</h2>

    <form method="post">
      <table>

        <tr>
          <td class="label">List Name</td>
          <td>
            <input type="text" name="list_name" placeholder="What is this list called?" value="">
          </td>
        </tr>

      </table>

      <input type="submit">
    </form>

  </body>
</html>