<!DOCTYPE html>

<html>
  <head>
    <title>List: {{list_data["name"]}}</title>
  </head>

  <body>
    <h1>Here's your list, {{list_data["owner"]}}</h1>
    <h2>{{list_data["name"]}}:</h2>

    <ul>
        % for item in list_data["items"]:
        <li>{{item["status"]}} | {{item["description"]}}</li>
        % end
    </ul>

  </body>
</html>