import datetime
import pymongo

class Lists:

    def __init__(self, db):
        self.db = db
        self.lists = self.db.lists

    def add_list(self, owner, list_name):
        list_id = Lists.get_list_id(owner, list_name)
        insert_d = {
            "_id": list_id,
            "owner": owner,
            "name": list_name,
            "items": []
        }

        try:
            self.lists.insert_one(insert_d)
        except pymongo.errors.OperationFailure:
            print "Mongo error in Lists.add_list"
            return False

        return True

    def add_item_to_list(self, owner, list_name, item_description):
        list_id = Lists.get_list_id(owner, list_name)
        filter_d = {
            "_id": list_id,
            "name": list_name,
            "owner": owner
        }

        update_d = {
            "$push": {
                "items": {
                    "description": item_description,
                    "status": False
                }
            }
        }

        try:
            self.lists.update_one(filter_d, update_d, upsert=True)
        except pymongo.errors.OperationFailure:
            print "Mongo error in Lists.add_item"
            return False

        return True

    def remove_item_from_list(self, owner, list_name):
        list_id = Lists.get_list_id(owner, list_name)
        filter_d = {
            "_id": list_id,
            "name": list_name,
            "owner": owner
        }

        try:
            result = self.lists.delete_one(filter_d)

            if result.deleted_count != 0:
                return False
        except pymongo.errors.OperationFailure:
            print "Mongo error in Lists.remove_item"
            return False

        return True

    def get_list(self, owner, list_name):
        list_id = Lists.get_list_id(owner, list_name)
        filter_d = {
            "_id": list_id
        }
        try:
            list_result = self.lists.find_one(filter_d)

            return list_result
        except pymongo.errors.OperationFailure:
            print "Mongo error in Lists.get_all_items"
            return False

    @staticmethod
    def get_list_id(owner, list_name):
        return owner + "_" + list_name
