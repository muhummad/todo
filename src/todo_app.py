from bottle import *
import pymongo

from todo_controller import Lists

app = Bottle()

# TODO: this is a placeholder until users and sessions can be implemented
owner = "muhummad.patel@gmail.com"

@app.get("/")
def index_get():
    return "Hello World"

@app.get("/list/add")
def add_get():
    return template("add_list")

@app.post("/list/add")
def add_post():
    list_name = request.forms.get("list_name")
    lists.add_list(owner, list_name)

    return redirect("/list/" + list_name)

@app.get("/list/<list_name>")
def todo_get(list_name):
    list_data = lists.get_list(owner, list_name)

    args = {"list_data": list_data}
    return template("list", args)

@app.get("/list/<list_name>/add")
def add_get(list_name):
    return template("add_item")

@app.post("/list/<list_name>/add")
def add_post(list_name):
    description = request.forms.get("description")
    lists.add_item_to_list(owner, list_name, description)

    return redirect("/list/" + list_name)


connection_string = "mongodb://localhost"
connection = pymongo.MongoClient(connection_string)
database = connection.todo

lists = Lists(database)

run(app, host="localhost", port=8080, debug=True, reloader=True)